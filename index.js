


var TelegramBot = require('node-telegram-bot-api');

lg = console.log;

token = process.env.BOT_TOKEN;

if (!token){
  lg("ERROR: No bot token.");
  process.exit(1);
}

// Setup polling way
var bot = new TelegramBot(token, {polling: true});

//var bot = new TelegramBot(token);

function startWith(s, st){
  return s.indexOf(st) === 0;
}

bot.on('message', function (msg) {
  var chat_id = msg.chat.id;
  var text = msg.text;
  
  //lg( msg );
  
  lg( "MESSAGE : " + chat_id + " : \"" + text + "\"" );
  
  //if (text.indexOf('/id') === 0){
  if ( startWith(text, '/id') ){
    bot.sendMessage(chat_id, String(chat_id));
  } /*else {
    bot.sendMessage(chat_id, '');
  }*/
  
});

//lg(bot.getUpdates());

//process.exit();

